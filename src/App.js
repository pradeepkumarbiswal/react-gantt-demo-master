import React, { Component } from 'react';
import Gantt from './components/Gantt';
import Toolbar from './components/Toolbar';
import MessageArea from './components/MessageArea';
import './App.css';
import Chart from "react-google-charts";

const data = {
  "data": [
    { "id": "10", "text": "Project #1", "start_date": "01-04-2019", "duration": 3, "order": 10, "progress": 0.4, "open": true, "color": "red" },
    { "id": "1", "text": "Task #1", "start_date": "02-04-2019", "duration": 1, "order": 10, "progress": 0.6, "parent": "10", "color": "pink" },
    { "id": "2", "text": "Task #2", "start_date": "01-04-2019", "duration": 2, "order": 20, "progress": 0.6, "parent": "10", "color": "pink" },
    { "id": "20", "text": "Project #2", "start_date": "01-04-2019", "duration": 3, "order": 10, "progress": 0.4, "type": "project", "open": true, "color": "red" },
    { "id": "3", "text": "Task #3", "start_date": "02-04-2019", "duration": 2, "order": 10, "progress": 0.6, "parent": "20", },
    { "id": "4", "text": "Task #4", "start_date": "01-04-2019", "duration": 2, "order": 20, "progress": 0.6, "parent": "20" }
  ],
  "links": []
};


const googleColumns = [
  { type: 'string', label: 'Task ID' },
  { type: 'string', label: 'Task Name' },
  { type: 'string', label: 'Resource' },
  { type: 'date', label: 'Start Date' },
  { type: 'date', label: 'End Date' },
  { type: 'number', label: 'Duration' },
  { type: 'number', label: 'Percent Complete' },
  { type: 'string', label: 'Dependencies' },
];

const googleRows = [
  [
    '2014Spring',
    'Spring 2014',
    'spring',
    new Date(2014, 2, 22),
    new Date(2014, 5, 20),
    null,
    100,
    null,
  ],
  [
    '2014Summer',
    'Summer 2014',
    'summer',
    new Date(2014, 5, 21),
    new Date(2014, 8, 20),
    null,
    100,
    null,
  ],
  [
    '2014Autumn',
    'Autumn 2014',
    'autumn',
    new Date(2014, 8, 21),
    new Date(2014, 11, 20),
    null,
    100,
    null,
  ],
  [
    '2014Winter',
    'Winter 2014',
    'winter',
    new Date(2014, 11, 21),
    new Date(2015, 2, 21),
    null,
    100,
    null,
  ],
  [
    '2015Spring',
    'Spring 2015',
    'spring',
    new Date(2015, 2, 22),
    new Date(2015, 5, 20),
    null,
    50,
    null,
  ],
  [
    '2015Summer',
    'Summer 2015',
    'summer',
    new Date(2015, 5, 21),
    new Date(2015, 8, 20),
    null,
    0,
    null,
  ],
  [
    '2015Autumn',
    'Autumn 2015',
    'autumn',
    new Date(2015, 8, 21),
    new Date(2015, 11, 20),
    null,
    0,
    null,
  ],
  [
    '2015Winter',
    'Winter 2015',
    'winter',
    new Date(2015, 11, 21),
    new Date(2016, 2, 21),
    null,
    0,
    null,
  ],
  [
    'Football',
    'Football Season',
    'sports',
    new Date(2014, 8, 4),
    new Date(2015, 1, 1),
    null,
    100,
    null,
  ],
  [
    'Baseball',
    'Baseball Season',
    'sports',
    new Date(2015, 2, 31),
    new Date(2015, 9, 20),
    null,
    14,
    null,
  ],
  [
    'Basketball',
    'Basketball Season',
    'sports',
    new Date(2014, 9, 28),
    new Date(2015, 5, 20),
    null,
    86,
    null,
  ],
  [
    'Hockey',
    'Hockey Season',
    'sports',
    new Date(2014, 9, 8),
    new Date(2015, 5, 21),
    null,
    89,
    null,
  ],
]

class App extends Component {
  state = {
    currentZoom: 'Days',
    messages: []
  };

  addMessage(message) {
    const maxLogLength = 5;
    const newMessate = { message };
    const messages = [
      newMessate,
      ...this.state.messages
    ];

    if (messages.length > maxLogLength) {
      messages.length = maxLogLength;
    }
    this.setState({ messages });
  }

  logDataUpdate = (type, action, item, id) => {
    let text = item && item.text ? ` (${item.text})` : '';
    let message = `${type} ${action}: ${id} ${text}`;
    if (type === 'link' && action !== 'delete') {
      message += ` ( source: ${item.source}, target: ${item.target} )`;
    }
    this.addMessage(message);
  }

  handleZoomChange = (zoom) => {
    this.setState({
      currentZoom: zoom
    });
  }

  render() {
    const { currentZoom, messages } = this.state;
    return (
      <div>
        <div className="zoom-bar">
          <Toolbar
            zoom={currentZoom}
            onZoomChange={this.handleZoomChange}
          />
        </div>
        <div className="gantt-container">
          <Gantt
            tasks={data}
            zoom={currentZoom}
            onDataUpdated={this.logDataUpdate}
          />
        </div>
        <Chart
          width={'100%'}
          height={'400px'}
          chartType="Gantt"
          loader={<div>Loading Chart</div>}
          data={[googleColumns,...googleRows]}
          options={{
            height: 400,
            gantt: {
              trackHeight: 30,
            },
          }}
          rootProps={{ 'data-testid': '2' }}
        />
      </div>
    );
  }
}

export default App;

